package view;

import patterns.decoratorPattern.BoldDecoration;
import patterns.decoratorPattern.ConsoleText;
import patterns.decoratorPattern.PlainConsoleText;
import patterns.decoratorPattern.UnderlineDecoration;
import patterns.factoryPattern.MultiplicationStrategyFactory;
import patterns.singletonPattern.Calculator;
import model.Constants;
import model.Data;
import patterns.strategyPattern.MultiplicationStrategy;

import java.util.Random;
import java.util.Scanner;

public class View
{
	private Data currentEquation;
	MultiplicationStrategyFactory strategyFactory = new MultiplicationStrategyFactory();
	String textColor = Constants.BLUE;
	boolean underlinedText = false;
	boolean boldText = false;
	
	private Scanner scanner;


	//
	public void firstScreen()
	{
		String equation = null;
		
		while (equation == null)
		{
			scanner = new Scanner(System.in);
			
			printWithDecorations("Choose equation: SUM, MULTIPLE, DIVIDED, SUBTRACT, SQRT");
			printWithDecorations("Type CONF to enter configuration menu");

			// changes the letter to Upper Case when we type in Lower Case
			equation = scanner.nextLine().toUpperCase();

			//when CONF command entered configuration screen is called
			if (equation.equals("CONF"))
			{
				equation = null;
				configScreen();
				continue;
			}

			// invalid text is entered
			else if (!Constants.validOperations.contains(equation))
			{
				System.out.println("Invalid operation!");
				equation = null;
				continue;
			}
			
			System.out.println("Enter first number:");
			
			double a = scanner.nextDouble();
			double b = 0;
			
			if (!equation.equals("SQRT"))
			{
				System.out.println("Enter second number:");
				b = scanner.nextDouble();
			}
			
			currentEquation = new Data();
			currentEquation.setA((int) a);
			currentEquation.setB((int) b);
			currentEquation.setEquation(equation);
		}
	}

	// final result screen demonstrated
	public void resultScreen(double result)
	{
		System.out.println("The result is " + result + "\n");
	}


	public void configScreen()
	{
		printWithDecorations("-- CONFIG MODE ENABLED --");
		
		// Changing multiplication strategy
		printWithDecorations("Current multiplication strategy: " + Calculator.getMultiplicationStrategy().getStrategyName());
		printWithDecorations("Would you like to change the multiplication strategy? (y/n)");
		
		while (true)
		{
			String input = scanner.nextLine().toLowerCase();
			
			if (input.equals("y"))
			{
				printWithDecorations("Choose one of the following: SIMPLE, KARATSUBA");
				
				while (true)
				{
					// changes the letter to Upper Case when we type in Lower Case
					String str = scanner.nextLine().toUpperCase();

					// chooses concrete strategy (SIMPLE or KARATSUBA) according to entered command
					MultiplicationStrategy strategy = strategyFactory.createMultiplicationStrategy(str);
					if (strategy != null)
					{
						Calculator.setMultiplicationStrategy(strategy);
						break;
					}
				}
				
				break;
			}
			else if (input.equals("n"))
			{
				break;
			}
		}
		
		// Changing text decoration
		if(boldText)
		{
			printWithDecorations("Current text decoration: BOLD " + textColor);
		}
		else if (underlinedText)
		{
			printWithDecorations("Current text decoration: UNDERLINED " + textColor);
		}
		else
		{
			printWithDecorations("Current text decoration: SIMPLE");
		}
		
		printWithDecorations("Would you like to change the text decorations? (y/n)");
		
		while (true)
		{
			String input = scanner.nextLine().toLowerCase();
			
			if (input.equals("y"))
			{
				printWithDecorations("Choose one of the following to change: BOLD, UNDERLINED, COLOR");
				
				while (true)
				{

					// changes the letter to Upper Case when we type in Lower Case
					String str = scanner.nextLine().toUpperCase();

					if(str.equals("BOLD"))
					{
						boldText = !boldText;
						underlinedText = false;
						break;
					}
					else if (str.equals("UNDERLINED"))
					{
						underlinedText = !underlinedText;
						boldText = false;
						break;
					}
					else if (str.equals("COLOR"))
					{
						String oldColor = textColor;
						// to make sure we don't choose the same color
						while(oldColor.equals(textColor))
						{
							String[] colors = Constants.regularColors;
							// selects random color from the list
							textColor = colors[new Random().nextInt(colors.length)];
						}
						break;
					}
				}
				break;
			}
			else if (input.equals("n"))
			{
				break;
			}
		}
	}
	
	public Data getCurrentEquation()
	{
		return currentEquation;
	}



	private void printWithDecorations(String str)
	{
		ConsoleText decoratedText = new PlainConsoleText(str, textColor);
		
		if(boldText)
		{
			decoratedText = new BoldDecoration(decoratedText);
		}
		if(underlinedText)
		{
			decoratedText = new UnderlineDecoration(decoratedText);
		}
		
		System.out.println(decoratedText);
	}
}
