package model;

public class Data
{
    private double a;

    private double b;

    private String equation;

    private double result;

    public Data()
    {
    }

    public double getA()
    {
        return a;
    }

    public void setA(double a)
    {
        this.a = a;
    }

    public double getB()
    {
        return b;
    }

    public void setB(double b)
    {
        this.b = b;
    }

    public String getEquation()
    {
        return equation;
    }

    public void setEquation(String equation)
    {
        this.equation = equation;
    }

    public double getResult()
    {
        return result;
    }

    public void setResult(double result)
    {
        this.result = result;
    }

    @Override
    public String toString()
    {
        switch (equation)
        {
            case "SQRT":
                return "√" + " " + a + " = " + result;
            case "SUM":
                return a + " " + "+" + " " + b + " = " + result;
            case "MULTIPLE":
                return a + " " + "*" + " " + b + " = " + result;
            case "DIVIDED":
                return a + " " + "/" + " " + b + " = " + result;
            case "SUBTRACT":
                return a + " " + "-" + " " + b + " = " + result;
            default:
                return "";
        }
    }
}
