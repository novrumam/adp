package model;

import java.util.ArrayList;

// this class contains some constant data that is required by other classes
public class Constants
{
    public static final String LOGGING_PATH = "logs.txt";
    public static ArrayList<String> validOperations = new ArrayList<>();

    static
    {
        validOperations.add("SUM");
        validOperations.add("MULTIPLE");
        validOperations.add("DIVIDED");
        validOperations.add("SUBTRACT");
        validOperations.add("SQRT");
    }

    // Reset
    public static final String RESET = "\033[0m";  // Text Reset

    // Regular Colors
    public static final String RED = "\033[0;31m";
    public static final String GREEN = "\033[0;32m";
    public static final String YELLOW = "\033[0;33m";
    public static final String BLUE = "\033[0;34m";
    public static final String CYAN = "\033[0;36m";
    public static final String WHITE = "\033[0;37m";
    public static final String[] regularColors = {RED, GREEN, YELLOW, BLUE, CYAN, WHITE};

    // Bold
    public static final String RED_BOLD = "\033[1;31m";
    public static final String GREEN_BOLD = "\033[1;32m";
    public static final String YELLOW_BOLD = "\033[1;33m";
    public static final String BLUE_BOLD = "\033[1;34m";
    public static final String CYAN_BOLD = "\033[1;36m";
    public static final String WHITE_BOLD = "\033[1;37m";

    // Underline
    public static final String RED_UNDERLINED = "\033[4;31m";
    public static final String GREEN_UNDERLINED = "\033[4;32m";
    public static final String YELLOW_UNDERLINED = "\033[4;33m";
    public static final String BLUE_UNDERLINED = "\033[4;34m";
    public static final String CYAN_UNDERLINED = "\033[4;36m";
    public static final String WHITE_UNDERLINED = "\033[4;37m";

    private Constants()
    {
    }
}
