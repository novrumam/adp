package patterns.singletonPattern;

import model.Data;
import patterns.strategyPattern.MultiplicationStrategy;
import patterns.strategyPattern.SimpleMultiplication;

public class Calculator
{
	private static Calculator instance = null;


	//instance of MultiplicationStrategy
	private static MultiplicationStrategy multiplicationStrategy = new SimpleMultiplication();


	// thread safe by using synchronized constructor
	public static synchronized Calculator getInstance()
	{
		if (instance == null)
		{
			instance = new Calculator();
		}
		return instance;
	}

	// does all operations
	public double add(double a, double b)
	{
		return a + b;
	}
	
	public double sub(double a, double b)
	{
		return a - b;
	}
	
	public double mult(double a, double b)
	{
		return (double) multiplicationStrategy.multiple(a, b);
	}
	
	public double divided(double a, double b)
	{
		return ((double) a) / b;
	}
	
	public double sqrt(double a)
	{
		return Math.sqrt(a);
	}





	public static void setMultiplicationStrategy(MultiplicationStrategy multiplicationStrategy)
	{
		Calculator.multiplicationStrategy = multiplicationStrategy;
	}
	
	public static MultiplicationStrategy getMultiplicationStrategy()
	{
		return multiplicationStrategy;
	}
}
