package patterns.strategyPattern;

public interface MultiplicationStrategy {

    //calculator uses this interface for multiplication operation: SIMPLE and KARATSUBA Strategies
    double multiple(double a, double b);
    String getStrategyName();
}
