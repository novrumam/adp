package patterns.strategyPattern;

public class SimpleMultiplication implements MultiplicationStrategy
{
	@Override

	// simple multiplication
	public double multiple(double a, double b)
	{
		return a * b;
	}
	
	@Override
	public String getStrategyName()
	{
		return "SIMPLE";
	}
}
