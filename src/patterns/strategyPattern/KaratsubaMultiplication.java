package patterns.strategyPattern;

public class KaratsubaMultiplication implements MultiplicationStrategy
{
	@Override
	public double multiple(double x, double y)
	{
		if (x < 10 && y < 10) // simple multiplication
		{
			return x * y;
		}

		// otherwise implements KARATSUBA Multiplication algorithm
		int length1 = numLength(x);
		int length2 = numLength(y);
		int maxLen = Math.max(length1, length2);
		int halfMaxLen = (maxLen / 2) + (maxLen % 2);
		
		long maxLenTen = (long) Math.pow(10, halfMaxLen);
		
		double a = x / maxLenTen;
		double b = x % maxLenTen;
		double c = y / maxLenTen;
		double d = y % maxLenTen;
		
		double z0 = multiple(a, c);
		double z1 = multiple(a + b, c + d);
		double z2 = multiple(b, d);
		
		return (z0 * (long) Math.pow(10, halfMaxLen * 2) +
				((z1 - z0 - z2) * (long) Math.pow(10, halfMaxLen) + z2));
	}
	
	public static int numLength(double n)
	{
		int noLen = 0;
		while (n > 0) {
			noLen++;
			n /= 10;
		}
		return noLen;
	}
	
	@Override
	public String getStrategyName()
	{
		return "KARATSUBA";
	}
}
