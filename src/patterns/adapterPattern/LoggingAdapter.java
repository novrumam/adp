package patterns.adapterPattern;

import model.Constants;
import model.Data;

import java.io.IOException;

public class LoggingAdapter
{
	private final FileWritingService adaptee = new FileWritingService();


	//changes results into String and writes in a file
	public void log(Data data)
	{
		try
		{
			adaptee.writeToFile(data.toString(), Constants.LOGGING_PATH);
		}
		catch (IOException e)
		{
			System.out.println("Logging error");
		}
	}
}
