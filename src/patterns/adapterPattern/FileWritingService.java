package patterns.adapterPattern;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class FileWritingService
{
	public void writeToFile(String content, String filePath) throws IOException
	{

		//path operation results to a file
		FileWriter fileWriter = new FileWriter(filePath, true);
		PrintWriter printWriter = new PrintWriter(fileWriter);
		printWriter.append(content).append("\n");
		printWriter.close();
	}
}
