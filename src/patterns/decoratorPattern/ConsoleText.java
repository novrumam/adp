package patterns.decoratorPattern;

public abstract class ConsoleText
{

	public abstract String toString();
	public abstract String getColor();
	public abstract void setColor(String colorString);
}
