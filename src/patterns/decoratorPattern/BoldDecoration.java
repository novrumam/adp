package patterns.decoratorPattern;

import model.Constants;

public class BoldDecoration extends ConsoleTextDecoration
{
	
	public BoldDecoration(ConsoleText consoleText)
	{
		super(consoleText);
	}
	
	@Override
	public String toString()
	{
		String textDecoration;


		//checks if text is BOLD
		switch (super.consoleText.getColor())
		{
			//OR statements
			case Constants.RED:
			case Constants.RED_BOLD:
			case Constants.RED_UNDERLINED:
				textDecoration = Constants.RED_BOLD;
				break;
			case Constants.GREEN:
			case Constants.GREEN_BOLD:
			case Constants.GREEN_UNDERLINED:
				textDecoration = Constants.GREEN_BOLD;
				break;
			case Constants.YELLOW:
			case Constants.YELLOW_BOLD:
			case Constants.YELLOW_UNDERLINED:
				textDecoration = Constants.YELLOW_BOLD;
				break;
			case Constants.BLUE:
			case Constants.BLUE_BOLD:
			case Constants.BLUE_UNDERLINED:
				textDecoration = Constants.BLUE_BOLD;
				break;
			case Constants.CYAN:
			case Constants.CYAN_BOLD:
			case Constants.CYAN_UNDERLINED:
				textDecoration = Constants.CYAN_BOLD;
				break;
			case Constants.WHITE:
			case Constants.WHITE_BOLD:
			case Constants.WHITE_UNDERLINED:
				textDecoration = Constants.WHITE_BOLD;
				break;
			default:
				textDecoration = "";
		}
		
		setColor(textDecoration);
		
		return super.consoleText.toString();
	}
}
