package patterns.decoratorPattern;

import model.Constants;

public class PlainConsoleText extends ConsoleText
{
	String text;
	String color;

	//changing color of the text
	public PlainConsoleText(String text, String color)
	{
		this.text = text;
		this.color = color;
	}
	
	@Override
	public String toString()
	{
		return color + text + Constants.RESET;
	}
	
	@Override
	public String getColor()
	{
		return color;
	}
	
	@Override
	public void setColor(String color)
	{
		this.color = color;
	}
}
