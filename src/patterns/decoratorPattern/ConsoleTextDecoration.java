package patterns.decoratorPattern;

public abstract class ConsoleTextDecoration extends ConsoleText
{
	ConsoleText consoleText;


	public ConsoleTextDecoration(ConsoleText consoleText)
	{
		this.consoleText = consoleText;
	}
	
	public abstract String toString();
	
	@Override
	public String getColor()
	{
		return consoleText.getColor();
	}
	
	@Override
	public void setColor(String colorString)
	{
		this.consoleText.setColor(colorString);
	}
}
