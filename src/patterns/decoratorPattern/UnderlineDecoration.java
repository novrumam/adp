package patterns.decoratorPattern;

import model.Constants;

public class UnderlineDecoration extends ConsoleTextDecoration
{
	
	public UnderlineDecoration(ConsoleText consoleText)
	{
		super(consoleText);
	}
	
	@Override
	public String toString()
	{
		String textDecoration;
		
		switch (super.consoleText.getColor())
		{
			//OR statement
			case Constants.RED:
			case Constants.RED_BOLD:
			case Constants.RED_UNDERLINED:
				textDecoration = Constants.RED_UNDERLINED;
				break;
			case Constants.GREEN:
			case Constants.GREEN_BOLD:
			case Constants.GREEN_UNDERLINED:
				textDecoration = Constants.GREEN_UNDERLINED;
				break;
			case Constants.YELLOW:
			case Constants.YELLOW_BOLD:
			case Constants.YELLOW_UNDERLINED:
				textDecoration = Constants.YELLOW_UNDERLINED;
				break;
			case Constants.BLUE:
			case Constants.BLUE_BOLD:
			case Constants.BLUE_UNDERLINED:
				textDecoration = Constants.BLUE_UNDERLINED;
				break;
			case Constants.CYAN:
			case Constants.CYAN_BOLD:
			case Constants.CYAN_UNDERLINED:
				textDecoration = Constants.CYAN_UNDERLINED;
				break;
			case Constants.WHITE:
			case Constants.WHITE_BOLD:
			case Constants.WHITE_UNDERLINED:
				textDecoration = Constants.WHITE_UNDERLINED;
				break;
			default:
				textDecoration = "";
		}
		
		setColor(textDecoration);
		
		return super.consoleText.toString();
	}
	
	@Override
	public void setColor(String colorString)
	{
		this.consoleText.setColor(colorString);
	}
}
