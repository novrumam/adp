package patterns.factoryPattern;

import patterns.strategyPattern.KaratsubaMultiplication;
import patterns.strategyPattern.MultiplicationStrategy;
import patterns.strategyPattern.SimpleMultiplication;

public class MultiplicationStrategyFactory
{
	public MultiplicationStrategy createMultiplicationStrategy(String strategyName)
	{

		// checks cases if "SIMPLE" is entered class returns SimpleMultiplication, otherwise KaratsubaMultiplication
		// changes by entering CONF command in console
		switch (strategyName)
		{
			case "SIMPLE":
				return new SimpleMultiplication();
			case "KARATSUBA":
				return new KaratsubaMultiplication();
			default:
				return null;
		}
	}
}
