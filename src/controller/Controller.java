package controller;

import model.Data;

import patterns.adapterPattern.LoggingAdapter;
import patterns.factoryPattern.MultiplicationStrategyFactory;
import patterns.singletonPattern.Calculator;
import view.View;

public class Controller
{
    View view = new View();
    LoggingAdapter logger = new LoggingAdapter();

    // contains the main loop of the program
    // acts as a brdge between Model and View

    public void start()
    {
        while (true)
        {
            view.firstScreen();

            Data input = view.getCurrentEquation();
            Calculator calculator = Calculator.getInstance();
            Double result = null;

            if (input.getEquation().equals("SUM"))
            {
                result = calculator.add(input.getA(), input.getB());
            }
            else if (input.getEquation().equals("MULTIPLE"))
            {
                result = calculator.mult(input.getA(), input.getB());
            }
            else if (input.getEquation().equals("SUBTRACT"))
            {
                result = calculator.sub(input.getA(), input.getB());
            }
            else if (input.getEquation().equals("DIVIDED"))
            {
                result = calculator.divided(input.getA(), input.getB());
            }
            else if (input.getEquation().equals("SQRT"))
            {
                result = calculator.sqrt(input.getA());
            }
            else
            {
                System.out.println("An error occurred :(");
            }

            if(result != null)
            {
                input.setResult(result);
                logger.log(input);
                view.resultScreen(result);
            }
        }
    }
}

