CALCULATOR

Final Project in Architecture and Design Patterns

Used Patterns: 
* Strategy Pattern - calculator has two types of multiplication operation: SIMPLE and KARATSUBA
* Singleton Pattern - performs all operations of calculator
* Factory Pattern - produces an appropriate instance of MultiplicationStrategy based on a given String
* Adapter Pattern - writes all calculation results in a txt file as a text
* Decorator Pattern - decorates texts in console with COLOR, makes UNDERLINED and BOLD